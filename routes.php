<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:38
 */

use Pecee\SimpleRouter\SimpleRouter;
require_once 'APIMiddleware.php';
require_once 'controllers/LoginController.php';
require_once 'controllers/DefaultController.php';
require_once 'controllers/SubscriberController.php';
require_once 'controllers/meetupController.php';

// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/
$prefix = '/API';

SimpleRouter::group(['prefix' => $prefix], function () {
    SimpleRouter::post('/login', 'LoginController@login')->name('login');
    SimpleRouter::get('/', 'DefaultController@defaultAction');
    SimpleRouter::post('/meetup', 'meetupController@addMeetup');
    SimpleRouter::get('/meetup', 'meetupController@displayAll');
    SimpleRouter::delete('/meetup/{id}', 'meetupController@deleteMeetup');
    SimpleRouter::get('/meetup/{id}', 'meetupController@displayOne');
    SimpleRouter::put('/meetup/{id}', 'meetupController@updateById');
});