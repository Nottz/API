<?php 

class Location {

    private $bdd;

    function __construct() {
        $user = 'root';
        $password = 'qwertybitch';
        $host = 'localhost';
        $database = 'API';

        $this->bdd = new PDO("mysql:host=$host; dbname=$database; charset=utf8", $user, $password);
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    private $address;
    private $city;
    private $zip_code;
    private $description;

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getZip_code() {
        return $this->zip_code;
    }

    public function setZip_code($zip_code) {
        $this->zip_code = $zip_code;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

}


?>