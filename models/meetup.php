<?php 

class Meetup {

        private $bdd;

    function __construct() {
        $user = 'root';
        $password = 'qwertybitch';
        $host = 'localhost';
        $database = 'API';

        $this->bdd = new PDO("mysql:host=$host; dbname=$database; charset=utf8", $user, $password);
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    
    //  Function pour créer un meetup
    public function addMeetup($title, $date, $description, $location) {
        $request = $this->bdd->prepare('INSERT INTO meetup (title, date, description, location) VALUES(:title, :date, :description, :location)');
        $request->execute(['title' => $title,
            'date' => $date,
            'description' => $description,
            'location' => $location,]);

            return [
                'id' => $this->bdd->lastInsertId(),
                'title' => $title,
                'date' => $date,
                'description' => $description,
                'location' => $location,
            ];
    }
    
    public function displayOne($id){
        $request = $this->bdd->prepare('SELECT * FROM meetup WHERE id = :id');
        $request->execute(['id' => $id]);
        $fetch = $request->fetch();
        return $fetch;
    }
    
    public function deleteMeetup($id) {
        $request = $this->bdd->prepare('DELETE FROM meetup WHERE id = :id');
        $request->execute(['id'=>$id]);
    }

    public function update($id, $title,$date, $description, $location) {
    $request = $this->bdd->prepare('UPDATE meetup SET title=:title, location=:location,description=:description, date=:date WHERE id=:id');
    $request->execute(['id'=> $id,
        'title' => $title,
        'date' => $date,
        'description' => $description,
        'location' => $location,
        ]);
    }

    public function displayAll() {
        $request = $this->bdd->prepare('SELECT * FROM meetup');
        $request->execute();
        $fetch = $request->fetchAll(PDO::FETCH_ASSOC);
        return $fetch;
    }
}

?>