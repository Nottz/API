<?php

use Pecee\SimpleRouter\SimpleRouter;
require_once 'controllers/Controller.php';

class LoginController extends Controller {
    function login() {
        if (isset($_POST['login']) && isset($_POST['passwd']) && $_POST['login'] === 'simplon' && $_POST['passwd'] === '2018') {
            SimpleRouter::csrfVerifier(new Pecee\Http\Middleware\BaseCsrfVerifier());
            $token = SimpleRouter::router()->getCsrfVerifier()->getTokenProvider()->getToken();
            return json_encode(['login' => 'success', 'token' => $token]);
        } else {
            http_response_code(401);
            return json_encode(['login' => 'error']);
        }
    }
}