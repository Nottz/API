<?php

require_once __DIR__.'/../models/meetup.php';
require_once __DIR__.'/Controller.php';

class MeetupController extends Controller {
    
    public function addMeetup(){
        $title = $_POST['title'];
        $date = $_POST['date'];
        $description = $_POST['description'];
        $location = $_POST['location'];
        $data = new Meetup;
        $datas = $data->addMeetup($title, $date, $description, $location);
        return json_encode($datas);
    }

    public function displayAll(){
        $data = new Meetup;
        $datas = $data->displayAll();
        return json_encode($datas);
    }

    public function deleteMeetup($id){
        $data = new Meetup;
        $datas = $data->deleteMeetup($id);
    }

    public function updateById($id){
        $put = $this->getHttpData();
        $id = $put['id'];
        $title = $put['title'];
        $date = $put['date'];
        $description = $put['description'];
        $location = $put['location'];
        
        $data = new Meetup;
        $datas = $data->update($id, $title, $date, $description, $location);
        return json_encode($datas);
    }

    public function displayOne($id) {
        $data = new Meetup;
        $datas = $data->displayOne($id);
        return json_encode($datas);
    }
}